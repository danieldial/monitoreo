/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.monitoreo.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Estefa Muñoz Diaz
 */
@Entity
@Table(name = "cetrodatos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cetrodatos.findAll", query = "SELECT c FROM Cetrodatos c"),
    @NamedQuery(name = "Cetrodatos.findByIdcendat", query = "SELECT c FROM Cetrodatos c WHERE c.idcendat = :idcendat"),
    @NamedQuery(name = "Cetrodatos.findByFecha", query = "SELECT c FROM Cetrodatos c WHERE c.fecha = :fecha"),
    @NamedQuery(name = "Cetrodatos.findByEstado", query = "SELECT c FROM Cetrodatos c WHERE c.estado = :estado")})
public class Cetrodatos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcendat")
    private Integer idcendat;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private boolean estado;
    @JoinColumn(name = "fk_codacti", referencedColumnName = "codacti")
    @ManyToOne(optional = false)
    private Actividad fkCodacti;
    @JoinColumn(name = "fk_codprog", referencedColumnName = "codprog")
    @ManyToOne(optional = false)
    private Programacion fkCodprog;

    public Cetrodatos() {
    }

    public Cetrodatos(Integer idcendat) {
        this.idcendat = idcendat;
    }

    public Cetrodatos(Integer idcendat, Date fecha, boolean estado) {
        this.idcendat = idcendat;
        this.fecha = fecha;
        this.estado = estado;
    }

    public Integer getIdcendat() {
        return idcendat;
    }

    public void setIdcendat(Integer idcendat) {
        this.idcendat = idcendat;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Actividad getFkCodacti() {
        return fkCodacti;
    }

    public void setFkCodacti(Actividad fkCodacti) {
        this.fkCodacti = fkCodacti;
    }

    public Programacion getFkCodprog() {
        return fkCodprog;
    }

    public void setFkCodprog(Programacion fkCodprog) {
        this.fkCodprog = fkCodprog;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcendat != null ? idcendat.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cetrodatos)) {
            return false;
        }
        Cetrodatos other = (Cetrodatos) object;
        if ((this.idcendat == null && other.idcendat != null) || (this.idcendat != null && !this.idcendat.equals(other.idcendat))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.edu.monitoreo.entidades.Cetrodatos[ idcendat=" + idcendat + " ]";
    }
    
}
