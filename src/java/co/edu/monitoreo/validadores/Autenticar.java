/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.monitoreo.validadores;



import co.edu.monitoreo.entidades.Usuario;
import co.edu.monitoreo.excepciones.EntidadesException;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;


public class Autenticar 
{
    
    public static void validarAutenticacion(String user, String passwd, EntityManager em) throws EntidadesException
    {
        Query q = em.createNamedQuery("Usuario.findByCodusua", Usuario.class).setParameter("codusua", Integer.parseInt(user));
        
        List<Usuario> listado = q.getResultList();
        
        if(listado.isEmpty())
        {
            throw new EntidadesException("No tiene permisos!");
        }
        else
        {
            if(!listado.get(0).getContrasena().equals(passwd))
            {
                throw new EntidadesException("No tiene permisos!");
            }
        }
    }
    
}
