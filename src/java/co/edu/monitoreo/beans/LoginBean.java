/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.monitoreo.beans;

import co.edu.monitoreo.controlador.UsuarioFacade;
import co.edu.monitoreo.entidades.Usuario;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Estefa Muñoz Diaz
 */
@Named(value = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    private String usuario;
    private String password;
    
    @EJB
    private UsuarioFacade conUsuario;
    
    private Usuario usuarioLogueado;
    /**
     * Creates a new instance of LoginBean
     */
    public LoginBean() {
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
    public String loguear()
    {
        usuarioLogueado = conUsuario.loguear(usuario);
        
        if(usuarioLogueado != null)
        {
            if(usuarioLogueado.getContrasena().equals(password))
            {
                return "ingresar";
            }
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Contraseña Incorrecta", "Contraseña Incorrecta"));
            return null;
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Usuario Incorrecto", "Usuario Incorrecto"));
        return null;
    }
    
}
