/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.monitoreo.servicios;

import co.edu.monitoreo.dto.RespuestaDto;
import co.edu.monitoreo.entidades.Actividad;
import co.edu.monitoreo.excepciones.EntidadesException;
import co.edu.monitoreo.validadores.Autenticar;
import co.edu.monitoreo.validadores.ValidadorActividad;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Estefa Muñoz Diaz
 */
@Stateless
@Path("co.edu.monitoreo.entidades.actividad")
public class ActividadFacadeREST extends AbstractFacade<Actividad> {

    @PersistenceContext(unitName = "monitoreoPU")
    private EntityManager em;

    public ActividadFacadeREST() {
        super(Actividad.class);
    }

    @POST
    
    @Consumes({MediaType.APPLICATION_JSON})
     public RespuestaDto createActividad(Actividad entity,@HeaderParam("user") String user, @HeaderParam("passwd") String passwd) {
        try {
            Autenticar.validarAutenticacion(user, passwd, em);
            ValidadorActividad.validarCodigoExistente(entity.getCodacti(),this );
            ValidadorActividad.ValidarCamposobligatorios(entity);
            
            super.create(entity);
            return new RespuestaDto("201", "Vehiculo Creado");
        } catch (EntidadesException ex) {
            return new RespuestaDto("406", ex.getMessage());  
        }
     }
    @PUT
    
    @Consumes({MediaType.APPLICATION_JSON})
    public RespuestaDto editActividad (Actividad entity,@HeaderParam("user") String user, @HeaderParam("passwd") String passwd) {
        try {
            Autenticar.validarAutenticacion(user, passwd, em);
            ValidadorActividad.validarCodigoExistente(entity.getCodacti(),this );
            ValidadorActividad.ValidarCamposobligatorios(entity);
            
            super.edit(entity);
            return new RespuestaDto("201", "Vehiculo modificado");
        } catch (EntidadesException ex) {
            return new RespuestaDto("406", ex.getMessage());  
        }
    }

    @DELETE
    @Path("{id}")
    public RespuestaDto removeActividad(@PathParam("id") Integer id,@HeaderParam("user") String user, @HeaderParam("passwd") String passwd) {
        try {
            Autenticar.validarAutenticacion(user, passwd, em);
            ValidadorActividad.validarCodigoExistente(id,this );
           
            
            super.remove(super.find(id));
            return new RespuestaDto("201", "Eliminado Correctamente");
        } catch (EntidadesException ex) {
            return new RespuestaDto("406", ex.getMessage());  
        }
       
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_JSON})
    public List<Actividad> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Actividad> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
