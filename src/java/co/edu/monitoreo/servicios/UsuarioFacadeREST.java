/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.monitoreo.servicios;

import co.edu.monitoreo.dto.RespuestaDto;
import co.edu.monitoreo.entidades.Usuario;
import co.edu.monitoreo.excepciones.EntidadesException;
import co.edu.monitoreo.validadores.Autenticar;
import co.edu.monitoreo.validadores.ValidadorUsuario;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Estefa Muñoz Diaz
 */
@Stateless
@Path("co.edu.monitoreo.entidades.usuario")
public class UsuarioFacadeREST extends AbstractFacade<Usuario> {

    @PersistenceContext(unitName = "monitoreoPU")
    private EntityManager em;

    public UsuarioFacadeREST() {
        super(Usuario.class);
    }

    @POST
    
    @Consumes({MediaType.APPLICATION_JSON})
    public RespuestaDto createUsuario(Usuario entity,@HeaderParam("user") String user, @HeaderParam("passwd") String passwd) {
        try {
            Autenticar.validarAutenticacion(user, passwd, em);
            ValidadorUsuario.validarCodigoExistente(entity.getCodusua(),this );
            ValidadorUsuario.ValidarCamposobligatorios(entity);
            
            super.create(entity);
            return new RespuestaDto("201", "Usuario Creado");
        } catch (EntidadesException ex) {
            return new RespuestaDto("406", ex.getMessage());  
        }
        
        
    }

    @PUT
    
    @Consumes({MediaType.APPLICATION_JSON})
    public RespuestaDto editusuario(Usuario entity,@HeaderParam("user") String user, @HeaderParam("passwd") String passwd) {
        try {
            Autenticar.validarAutenticacion(user, passwd, em);
            ValidadorUsuario.validarCodigoExistente(entity.getCodusua(),this );
            ValidadorUsuario.ValidarCamposobligatorios(entity);
            
            super.edit(entity);
            return new RespuestaDto("201", "Usuario modificado");
        } catch (EntidadesException ex) {
            return new RespuestaDto("406", ex.getMessage());  
        }
        
    }

    @DELETE
    @Path("{id}")
    public RespuestaDto removeusuario(@PathParam("id") Integer id,@HeaderParam("user") String user, @HeaderParam("passwd") String passwd) {
        try {
            Autenticar.validarAutenticacion(user, passwd, em);
            ValidadorUsuario.validarCodigoExistente(id,this );
           
            
            super.remove(super.find(id));
            return new RespuestaDto("201", "Eliminado Correctamente");
        } catch (EntidadesException ex) {
            return new RespuestaDto("406", ex.getMessage());  
        }
       
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Usuario find(@PathParam("id") Integer id,@HeaderParam("user") String user, @HeaderParam("passwd") String passwd) {
        
       try {
            Autenticar.validarAutenticacion(user, passwd, em);
            return super.find(id);
        } catch (EntidadesException ex) {
            Logger.getLogger(UsuarioFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @GET
    
    @Produces({MediaType.APPLICATION_JSON})
    public List<Usuario> findAll(@HeaderParam("user") String user, @HeaderParam("passwd") String passwd) {
        try 
        {
            Autenticar.validarAutenticacion(user, passwd, em);
            return super.findAll();
        } 
        catch (EntidadesException ex) 
        {
            Logger.getLogger(UsuarioFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        //return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Usuario> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to,@HeaderParam("user") String user, @HeaderParam("passwd") String passwd) {
       try 
        {
            Autenticar.validarAutenticacion(user, passwd, em);
            return super.findRange(new int[]{from, to});
        } 
        catch (EntidadesException ex) {
            Logger.getLogger(UsuarioFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null; 
//return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST(@HeaderParam("user") String user, @HeaderParam("passwd") String passwd) {
         try 
        {
            Autenticar.validarAutenticacion(user, passwd, em);
            return String.valueOf(super.count());
        } 
        catch (EntidadesException ex) {
            Logger.getLogger(UsuarioFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "No tiene permisos";
//return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
