/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.monitoreo.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Estefa Muñoz Diaz
 */
@Entity
@Table(name = "notificacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Notificacion.findAll", query = "SELECT n FROM Notificacion n"),
    @NamedQuery(name = "Notificacion.findByIdnoti", query = "SELECT n FROM Notificacion n WHERE n.idnoti = :idnoti"),
    @NamedQuery(name = "Notificacion.findByFecha", query = "SELECT n FROM Notificacion n WHERE n.fecha = :fecha"),
    @NamedQuery(name = "Notificacion.findByTipoNotif", query = "SELECT n FROM Notificacion n WHERE n.tipoNotif = :tipoNotif"),
    @NamedQuery(name = "Notificacion.findByPlaca", query = "SELECT n FROM Notificacion n WHERE n.placa = :placa"),
    @NamedQuery(name = "Notificacion.findByRegisTelef", query = "SELECT n FROM Notificacion n WHERE n.regisTelef = :regisTelef"),
    @NamedQuery(name = "Notificacion.findByEstado", query = "SELECT n FROM Notificacion n WHERE n.estado = :estado")})
public class Notificacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idnoti")
    private Integer idnoti;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "tipo_notif")
    private String tipoNotif;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "placa")
    private String placa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "regis_telef")
    private int regisTelef;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private boolean estado;
    @JoinColumn(name = "fk_codacti", referencedColumnName = "codacti")
    @ManyToOne(optional = false)
    private Actividad fkCodacti;
    @JoinColumn(name = "fk_codprog", referencedColumnName = "codprog")
    @ManyToOne(optional = false)
    private Programacion fkCodprog;
    @JoinColumn(name = "fk_codusua", referencedColumnName = "codusua")
    @ManyToOne(optional = false)
    private Usuario fkCodusua;

    public Notificacion() {
    }

    public Notificacion(Integer idnoti) {
        this.idnoti = idnoti;
    }

    public Notificacion(Integer idnoti, Date fecha, String tipoNotif, String placa, int regisTelef, boolean estado) {
        this.idnoti = idnoti;
        this.fecha = fecha;
        this.tipoNotif = tipoNotif;
        this.placa = placa;
        this.regisTelef = regisTelef;
        this.estado = estado;
    }

    public Integer getIdnoti() {
        return idnoti;
    }

    public void setIdnoti(Integer idnoti) {
        this.idnoti = idnoti;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getTipoNotif() {
        return tipoNotif;
    }

    public void setTipoNotif(String tipoNotif) {
        this.tipoNotif = tipoNotif;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public int getRegisTelef() {
        return regisTelef;
    }

    public void setRegisTelef(int regisTelef) {
        this.regisTelef = regisTelef;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Actividad getFkCodacti() {
        return fkCodacti;
    }

    public void setFkCodacti(Actividad fkCodacti) {
        this.fkCodacti = fkCodacti;
    }

    public Programacion getFkCodprog() {
        return fkCodprog;
    }

    public void setFkCodprog(Programacion fkCodprog) {
        this.fkCodprog = fkCodprog;
    }

    public Usuario getFkCodusua() {
        return fkCodusua;
    }

    public void setFkCodusua(Usuario fkCodusua) {
        this.fkCodusua = fkCodusua;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idnoti != null ? idnoti.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Notificacion)) {
            return false;
        }
        Notificacion other = (Notificacion) object;
        if ((this.idnoti == null && other.idnoti != null) || (this.idnoti != null && !this.idnoti.equals(other.idnoti))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.edu.monitoreo.entidades.Notificacion[ idnoti=" + idnoti + " ]";
    }
    
}
