/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.monitoreo.controlador;

import co.edu.monitoreo.entidades.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Estefa Muñoz Diaz
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {

    @PersistenceContext(unitName = "monitoreoPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }
    
    public Usuario loguear(String usuario)
    {
        Query q = em.createNamedQuery("Usuario.findByCodusua", Usuario.class).setParameter("codusua", Integer.parseInt(usuario));
        
        List<Usuario> ListaUsuario = q.getResultList();
        
        if(!ListaUsuario.isEmpty())
        {
            return ListaUsuario.get(0);
        }
        return null;
    }
    
}
