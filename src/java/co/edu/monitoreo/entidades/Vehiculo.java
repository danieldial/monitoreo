/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.monitoreo.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Estefa Muñoz Diaz
 */
@Entity
@Table(name = "vehiculo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vehiculo.findAll", query = "SELECT v FROM Vehiculo v"),
    @NamedQuery(name = "Vehiculo.findByCodvehi", query = "SELECT v FROM Vehiculo v WHERE v.codvehi = :codvehi"),
    @NamedQuery(name = "Vehiculo.findByPlaca", query = "SELECT v FROM Vehiculo v WHERE v.placa = :placa"),
    @NamedQuery(name = "Vehiculo.findByCaracteristicas", query = "SELECT v FROM Vehiculo v WHERE v.caracteristicas = :caracteristicas")})
public class Vehiculo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codvehi")
    private Integer codvehi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "placa")
    private String placa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "caracteristicas")
    private String caracteristicas;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkCodvehi")
    private List<Programacion> programacionList;
    @JoinColumn(name = "fk_codusua", referencedColumnName = "codusua")
    @ManyToOne(optional = false)
    private Usuario fkCodusua;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkCodvehi")
    private List<Actividad> actividadList;

    public Vehiculo() {
    }

    public Vehiculo(Integer codvehi) {
        this.codvehi = codvehi;
    }

    public Vehiculo(Integer codvehi, String placa, String caracteristicas) {
        this.codvehi = codvehi;
        this.placa = placa;
        this.caracteristicas = caracteristicas;
    }

    public Integer getCodvehi() {
        return codvehi;
    }

    public void setCodvehi(Integer codvehi) {
        this.codvehi = codvehi;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getCaracteristicas() {
        return caracteristicas;
    }

    public void setCaracteristicas(String caracteristicas) {
        this.caracteristicas = caracteristicas;
    }

    @XmlTransient
    public List<Programacion> getProgramacionList() {
        return programacionList;
    }

    public void setProgramacionList(List<Programacion> programacionList) {
        this.programacionList = programacionList;
    }

    public Usuario getFkCodusua() {
        return fkCodusua;
    }

    public void setFkCodusua(Usuario fkCodusua) {
        this.fkCodusua = fkCodusua;
    }

    @XmlTransient
    public List<Actividad> getActividadList() {
        return actividadList;
    }

    public void setActividadList(List<Actividad> actividadList) {
        this.actividadList = actividadList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codvehi != null ? codvehi.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vehiculo)) {
            return false;
        }
        Vehiculo other = (Vehiculo) object;
        if ((this.codvehi == null && other.codvehi != null) || (this.codvehi != null && !this.codvehi.equals(other.codvehi))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.edu.monitoreo.entidades.Vehiculo[ codvehi=" + codvehi + " ]";
    }
    
}
