/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.monitoreo.validadores;

import co.edu.monitoreo.entidades.Programacion;
import co.edu.monitoreo.excepciones.EntidadesException;
import co.edu.monitoreo.servicios.ProgramacionFacadeREST;

/**
 *
 * @author Estefa Muñoz Diaz
 */
public class ValidadorProgramacion {
    
    public static void validarCodigoExistente(int id,ProgramacionFacadeREST programacionFacade)throws EntidadesException{
        if(programacionFacade.find(id)!=null)
        {
          throw new EntidadesException("el id creado ya existe");
        }
        
        }
     public static void validarCodigoNoExistente(int id,ProgramacionFacadeREST programacionFacade)throws EntidadesException{
        if(programacionFacade.find(id)==null)
        {
          throw new EntidadesException("el id creado no existe");
        }
        }
      public static void ValidarCamposobligatorios (Programacion programacion)throws EntidadesException{
        if(programacion.getCodprog()== null || programacion.getCodprog().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo del codigo");
        }
         if(programacion.getFecha()== null || programacion.getFecha().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo del Nombre");
        }
          if(programacion.getDescripcion()== null || programacion.getDescripcion().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo de Dirreccion");
          
        } if(programacion.getHoraProg()== null || programacion.getHoraProg().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo de Dirreccion");
        }
          if(programacion.getDescripcion()== null || programacion.getDescripcion().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo de Dirreccion");
        }
        
          
             
    
}
    
}
