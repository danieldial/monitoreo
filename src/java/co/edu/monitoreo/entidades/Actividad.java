/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.monitoreo.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Estefa Muñoz Diaz
 */
@Entity
@Table(name = "actividad")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Actividad.findAll", query = "SELECT a FROM Actividad a"),
    @NamedQuery(name = "Actividad.findByCodacti", query = "SELECT a FROM Actividad a WHERE a.codacti = :codacti"),
    @NamedQuery(name = "Actividad.findByFecha", query = "SELECT a FROM Actividad a WHERE a.fecha = :fecha"),
    @NamedQuery(name = "Actividad.findByHoraActi", query = "SELECT a FROM Actividad a WHERE a.horaActi = :horaActi"),
    @NamedQuery(name = "Actividad.findByDescripcion", query = "SELECT a FROM Actividad a WHERE a.descripcion = :descripcion")})
public class Actividad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codacti")
    private Integer codacti;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "hora_acti")
    @Temporal(TemporalType.TIME)
    private Date horaActi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkCodacti")
    private List<Cetrodatos> cetrodatosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkCodacti")
    private List<Programacion> programacionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkCodacti")
    private List<Notificacion> notificacionList;
    @JoinColumn(name = "fk_codvehi", referencedColumnName = "codvehi")
    @ManyToOne(optional = false)
    private Vehiculo fkCodvehi;

    public Actividad() {
    }

    public Actividad(Integer codacti) {
        this.codacti = codacti;
    }

    public Actividad(Integer codacti, Date fecha, Date horaActi, String descripcion) {
        this.codacti = codacti;
        this.fecha = fecha;
        this.horaActi = horaActi;
        this.descripcion = descripcion;
    }

    public Integer getCodacti() {
        return codacti;
    }

    public void setCodacti(Integer codacti) {
        this.codacti = codacti;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getHoraActi() {
        return horaActi;
    }

    public void setHoraActi(Date horaActi) {
        this.horaActi = horaActi;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Cetrodatos> getCetrodatosList() {
        return cetrodatosList;
    }

    public void setCetrodatosList(List<Cetrodatos> cetrodatosList) {
        this.cetrodatosList = cetrodatosList;
    }

    @XmlTransient
    public List<Programacion> getProgramacionList() {
        return programacionList;
    }

    public void setProgramacionList(List<Programacion> programacionList) {
        this.programacionList = programacionList;
    }

    @XmlTransient
    public List<Notificacion> getNotificacionList() {
        return notificacionList;
    }

    public void setNotificacionList(List<Notificacion> notificacionList) {
        this.notificacionList = notificacionList;
    }

    public Vehiculo getFkCodvehi() {
        return fkCodvehi;
    }

    public void setFkCodvehi(Vehiculo fkCodvehi) {
        this.fkCodvehi = fkCodvehi;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codacti != null ? codacti.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Actividad)) {
            return false;
        }
        Actividad other = (Actividad) object;
        if ((this.codacti == null && other.codacti != null) || (this.codacti != null && !this.codacti.equals(other.codacti))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.edu.monitoreo.entidades.Actividad[ codacti=" + codacti + " ]";
    }
    
}
