/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.monitoreo.validadores;

import co.edu.monitoreo.entidades.Cetrodatos;
import co.edu.monitoreo.excepciones.EntidadesException;
import co.edu.monitoreo.servicios.CetrodatosFacadeREST;

/**
 *
 * @author Estefa Muñoz Diaz
 */
public class ValidadorCentroDatos {
    
    public static void validarCodigoExistente(int id,CetrodatosFacadeREST cetrodatosFacade)throws EntidadesException{
        if(cetrodatosFacade.find(id)!=null)
        {
          throw new EntidadesException("el id creado ya existe");
        }
        
        }
     public static void validarCodigoNoExistente(int id,CetrodatosFacadeREST cetrodatosFacade)throws EntidadesException{
        if(cetrodatosFacade.find(id)==null)
        {
          throw new EntidadesException("el id creado no existe");
        }
        }
      public static void ValidarCamposobligatorios (Cetrodatos centrodatos)throws EntidadesException{
        if(centrodatos.getIdcendat()== null || centrodatos.getIdcendat().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo del id");
        }
         if(centrodatos.getFecha()== null || centrodatos.getFecha().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo fecha");
        }
          
         
}
    
}
