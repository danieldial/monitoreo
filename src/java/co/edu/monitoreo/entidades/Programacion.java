/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.monitoreo.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Estefa Muñoz Diaz
 */
@Entity
@Table(name = "programacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Programacion.findAll", query = "SELECT p FROM Programacion p"),
    @NamedQuery(name = "Programacion.findByCodprog", query = "SELECT p FROM Programacion p WHERE p.codprog = :codprog"),
    @NamedQuery(name = "Programacion.findByFecha", query = "SELECT p FROM Programacion p WHERE p.fecha = :fecha"),
    @NamedQuery(name = "Programacion.findByHoraProg", query = "SELECT p FROM Programacion p WHERE p.horaProg = :horaProg"),
    @NamedQuery(name = "Programacion.findByDescripcion", query = "SELECT p FROM Programacion p WHERE p.descripcion = :descripcion"),
    @NamedQuery(name = "Programacion.findByEstado", query = "SELECT p FROM Programacion p WHERE p.estado = :estado")})
public class Programacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codprog")
    private Integer codprog;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "hora_prog")
    @Temporal(TemporalType.TIME)
    private Date horaProg;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private boolean estado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkCodprog")
    private List<Cetrodatos> cetrodatosList;
    @JoinColumn(name = "fk_codacti", referencedColumnName = "codacti")
    @ManyToOne(optional = false)
    private Actividad fkCodacti;
    @JoinColumn(name = "fk_codvehi", referencedColumnName = "codvehi")
    @ManyToOne(optional = false)
    private Vehiculo fkCodvehi;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkCodprog")
    private List<Notificacion> notificacionList;

    public Programacion() {
    }

    public Programacion(Integer codprog) {
        this.codprog = codprog;
    }

    public Programacion(Integer codprog, Date fecha, Date horaProg, String descripcion, boolean estado) {
        this.codprog = codprog;
        this.fecha = fecha;
        this.horaProg = horaProg;
        this.descripcion = descripcion;
        this.estado = estado;
    }

    public Integer getCodprog() {
        return codprog;
    }

    public void setCodprog(Integer codprog) {
        this.codprog = codprog;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getHoraProg() {
        return horaProg;
    }

    public void setHoraProg(Date horaProg) {
        this.horaProg = horaProg;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<Cetrodatos> getCetrodatosList() {
        return cetrodatosList;
    }

    public void setCetrodatosList(List<Cetrodatos> cetrodatosList) {
        this.cetrodatosList = cetrodatosList;
    }

    public Actividad getFkCodacti() {
        return fkCodacti;
    }

    public void setFkCodacti(Actividad fkCodacti) {
        this.fkCodacti = fkCodacti;
    }

    public Vehiculo getFkCodvehi() {
        return fkCodvehi;
    }

    public void setFkCodvehi(Vehiculo fkCodvehi) {
        this.fkCodvehi = fkCodvehi;
    }

    @XmlTransient
    public List<Notificacion> getNotificacionList() {
        return notificacionList;
    }

    public void setNotificacionList(List<Notificacion> notificacionList) {
        this.notificacionList = notificacionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codprog != null ? codprog.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Programacion)) {
            return false;
        }
        Programacion other = (Programacion) object;
        if ((this.codprog == null && other.codprog != null) || (this.codprog != null && !this.codprog.equals(other.codprog))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.edu.monitoreo.entidades.Programacion[ codprog=" + codprog + " ]";
    }
    
}
