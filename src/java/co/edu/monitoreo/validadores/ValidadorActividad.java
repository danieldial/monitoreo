/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.monitoreo.validadores;

import co.edu.monitoreo.entidades.Actividad;
import co.edu.monitoreo.excepciones.EntidadesException;
import co.edu.monitoreo.servicios.ActividadFacadeREST;

/**
 *
 * @author Estefa Muñoz Diaz
 */
public class ValidadorActividad {
    
    public static void validarCodigoExistente(int id,ActividadFacadeREST actividadFacade)throws EntidadesException{
        if(actividadFacade.find(id)!=null)
        {
          throw new EntidadesException("el id creado ya existe");
        }
        
        }
     public static void validarCodigoNoExistente(int id,ActividadFacadeREST actividadFacade)throws EntidadesException{
        if(actividadFacade.find(id)==null)
        {
          throw new EntidadesException("el id creado no existe");
        }
        }
      public static void ValidarCamposobligatorios (Actividad actividad)throws EntidadesException{
        if(actividad.getCodacti()== null || actividad.getCodacti().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo del codigo de la actividad");
        }
         if(actividad.getFecha()== null || actividad.getFecha().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo de la fecha de la actividad");
        }
          if(actividad.getHoraActi()== null || actividad.getHoraActi().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo de hora de la actividad");
        }
           if(actividad.getDescripcion()== null || actividad.getDescripcion().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo de la descripcion");
        }
             
    
}

    
}
