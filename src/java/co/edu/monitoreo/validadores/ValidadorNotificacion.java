/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.monitoreo.validadores;

import co.edu.monitoreo.entidades.Notificacion;
import co.edu.monitoreo.excepciones.EntidadesException;
import co.edu.monitoreo.servicios.NotificacionFacadeREST;

/**
 *
 * @author Estefa Muñoz Diaz
 */
public class ValidadorNotificacion {
    
    public static void validarCodigoExistente(int id,NotificacionFacadeREST notificacionFacade)throws EntidadesException{
        if(notificacionFacade.find(id)!=null)
        {
          throw new EntidadesException("el id creado ya existe");
        }
        
        }
     public static void validarCodigoNoExistente(int id,NotificacionFacadeREST notificacionFacade)throws EntidadesException{
        if(notificacionFacade.find(id)==null)
        {
          throw new EntidadesException("el id creado no existe");
        }
        }
      public static void ValidarCamposobligatorios (Notificacion notificacion)throws EntidadesException{
        if(notificacion.getIdnoti()== null || notificacion.getIdnoti().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo del id notificacion");
        }
         if(notificacion.getFecha()== null || notificacion.getFecha().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo de fecha");
        }
          if(notificacion.getPlaca()== null || notificacion.getPlaca().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo de Placa");
        }
          if(notificacion.getTipoNotif()== null || notificacion.getTipoNotif().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo de tipo de notificacion");
        }
          if(notificacion.getRegisTelef()== 0)
        {
          throw new EntidadesException("debe ingresar el campo telefonico");
        }
          
             
    
}

    
}
