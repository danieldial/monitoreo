/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.monitoreo.validadores;

import co.edu.monitoreo.controlador.UsuarioFacade;
import co.edu.monitoreo.entidades.Usuario;
import co.edu.monitoreo.excepciones.EntidadesException;
import co.edu.monitoreo.servicios.UsuarioFacadeREST;

/**
 *
 * @author Estefa Muñoz Diaz
 */
public class ValidadorUsuario  {
    
    public static void validarCodigoExistente(int id,UsuarioFacadeREST usuarioFacade)throws EntidadesException{
        if(usuarioFacade.find(id)!=null)
        {
          throw new EntidadesException("el id creado ya existe");
        }
        }
    
    public static void validarCodigoNoExistente(int id,UsuarioFacadeREST usuarioFacade)throws EntidadesException{
        if(usuarioFacade.find(id)==null)
        {
          throw new EntidadesException("el id creado no existe");
        }
        }
    
    public static void ValidarCamposobligatorios (Usuario usuario)throws EntidadesException{
        if(usuario.getCodusua()== null || usuario.getCodusua().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo del usuario");
        }
         if(usuario.getNombre()== null || usuario.getNombre().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo del Nombre");
        }
          if(usuario.getDireccion()== null || usuario.getDireccion().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo de Dirreccion");
        }
           if(usuario.getContrasena()== null || usuario.getContrasena().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo de Contraseña");
        }
            if(usuario.getEmail()== null || usuario.getEmail().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo del Email");
        }
            
    }
    
    
    
}

