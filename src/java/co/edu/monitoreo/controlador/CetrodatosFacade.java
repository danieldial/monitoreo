/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.monitoreo.controlador;

import co.edu.monitoreo.entidades.Cetrodatos;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Estefa Muñoz Diaz
 */
@Stateless
public class CetrodatosFacade extends AbstractFacade<Cetrodatos> {

    @PersistenceContext(unitName = "monitoreoPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CetrodatosFacade() {
        super(Cetrodatos.class);
    }
    
}
