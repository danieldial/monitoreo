/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.monitoreo.validadores;

import co.edu.monitoreo.excepciones.EntidadesException;
import co.edu.monitoreo.servicios.VehiculoFacadeREST;
import co.edu.monitoreo.entidades.Vehiculo;


/**
 *
 * @author Estefa Muñoz Diaz
 */
public class ValidadorVehiculo {
    
    public static void validarCodigoExistente(int id,VehiculoFacadeREST vehiculoFacade)throws EntidadesException{
        if(vehiculoFacade.find(id)!=null)
        {
          throw new EntidadesException("el id creado ya existe");
        }
        
        }
     public static void validarCodigoNoExistente(int id,VehiculoFacadeREST vehiculoFacade)throws EntidadesException{
        if(vehiculoFacade.find(id)==null)
        {
          throw new EntidadesException("el id creado no existe");
        }
        }
      public static void ValidarCamposobligatorios (Vehiculo vehiculo)throws EntidadesException{
        if(vehiculo.getCodvehi()== null || vehiculo.getCodvehi().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo del usuario");
        }
         if(vehiculo.getPlaca()== null || vehiculo.getPlaca().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo del Nombre");
        }
          if(vehiculo.getCaracteristicas()== null || vehiculo.getCaracteristicas().equals(""))
        {
          throw new EntidadesException("debe ingresar el campo de Dirreccion");
        }
             
    
}

}